package Reporter;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer');

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('apps#home');

  $r->get('/reporting')->to('apps#reporting');
  $r->get('/tracking')->to('apps#tracking');

  $r->get('/reports')->to('reports#list');
  $r->post('/reports')->to('reports#create');

  $r->get('/reports/:id')->to('reports#show');
  $r->put('/reports/:id')->to('reports#update');
  $r->delete('/reports/:id')->to('reports#delete');

  #$self->plugin('resourceful_routes');
  #$self->resources('reports');
}

1;
