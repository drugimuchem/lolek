package Reporter::Reports;
use Mojo::Base 'Mojolicious::Controller';
use Reporter::Model::Report;


sub index { }

sub list {
    my $self = shift;
    my $reports = Reporter::Model::Report->get();
    $self->respond_to(
        json => { json => $reports },
        html => { text => 'dupa'}
    )
}

sub show {
    my $self = shift;

    $self->respond_to(
        json => { json => Reporter::Model::Report::find_by_id($self->param('id')) },
        html => { text => 'dupa'}
    )
}

sub create {
    my $self = shift;
    Reporter::Model::Report::create( $self->req->json);
    $self->render_text(Reporter::Model::Report->count());
}
sub update {
    my $self = shift;
    my $report = Reporter::Model::Report::update($self->param('id'),$self->req->json);
    $self->respond_to(
        json => { json => $report }
    );
}
sub delete {
    my $self = shift;
}



1;
