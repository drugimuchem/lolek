
/*
 *    Converts underscored string to capitalized camelcased string.
 *    Created for translating template filenames to corresponding classnames.
 *    eg:
 *    my_string => MyString
 *
 *    @param {String} str Underscored lowercase string
 *    @returns {String} Capitalized string
 */
function dashToCapitalizedCamel(str) {
    return str.replace(/(?:^|-)(\w)/g, function(match,subMatch) {
        return subMatch.toUpperCase();
    });
}
