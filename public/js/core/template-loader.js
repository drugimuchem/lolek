

window.templateLoader = {

/*
 *    Loads templates from external files directly to View classes.
 *    Template filenames are mapped to view class names and template directories to class types,
 *    eg:
 *
 *    template                                    View class 
 *    ----------------------------------------------------------------
 *    DOCUMENT_ROOT/tpl/views/model_simple.html   OWA.View.ModelSimple
 *    DOCUMENT_ROOT/tpl/pages/another_page.html   OWA.Page.AnotherPage
 *
 *
 *    @param {Array} views An array of template names, eg: ['views/report_form','partial/search_box']
 *    @param {Function} callback Function to call when all templates are ready to use
 */
    load: function(views, callback) {

        var 
        deferreds = [], // collect templates to load
        CLASS_TYPE_FOR_TPL = {
            'pages' : 'Page',
            'views' : 'View',
        };

        // store once loaded templates
        window['templates'] = window['templates'] || {};

        $.each(views, function(index, tplPath) {
            var 
            tplSrc    = 'tpl/' + tplPath + '.html',
            tplModule = tplPath.split('/'),
            tplType   = tplModule[0],
            tplClass  = dashToCapitalizedCamel(tplModule[1]);

            templates[tplType] = templates[tplType] || {};

            if (typeof templates[tplType][tplClass] == 'string') return; // template already loaded

            deferreds.push($.get(tplSrc, function(data) {
                var classType = CLASS_TYPE_FOR_TPL[tplType];
                if (typeof classType == 'string') {
                    var bbClass = window.OWA[classType][tplClass];
                    if (bbClass) {
                        bbClass.prototype.template = _.template(data);
                    } else {
                        throw("class " + bbNamespace + "." + tplClass + " not found");
                    }
                } else {
                    templates[tplType][tplClass] = _.template(data);
                }

            }, 'html'));

        });

        $.when.apply(null, deferreds).done(callback);
    }

};

