OWA.View.Report = Backbone.View.extend({

    className : 'list-item',

    initialize : function() {
        this.model.on('change', this.render, this);
    },

    render : function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});
