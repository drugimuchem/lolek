OWA.View.ReportForm = Backbone.View.extend({

    events : {
        'change input' : 'updateFromInput',
        'click span'   : 'close'
    },

    initialize : function() {
        this.model.on('change', this.render, this);
    },

    render : function() {
        var self = this;
        this.$el.html(this.template(this.model.toJSON()));
		this.$el.dialog({
			modal : true,
            close : function() {
                console.log(this);
                self.trigger('cancel')
            }
		});
        return this;
    },

    updateFromInput : function() {
        var val = this.input.val().trim()
        this.model.set('name',val);
    },

    close : function() {
    }
})
