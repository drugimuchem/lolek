OWA.Router.Reporting = Backbone.Router.extend({
	routes : {
		''        : 'index',
		'new'     : 'new',
		'edit/:id': 'edit'
	}
});
