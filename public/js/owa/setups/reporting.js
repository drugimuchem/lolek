

templateLoader.load(["views/report", "views/report-form", "pages/reporting"], function() {
        app.page = new OWA.Page.Reporting();

        app.page.on('ready', function(){
            app.router = new OWA.Router.Reporting();

            app.router.on('route:index', function(){
                console.log('index');
                if ( app.reportForm ) {
                    app.reportForm.remove();
                }
            });

            app.router.on('route:new', function(){
                console.log('route:new');
                //TODO keep one instance of OWA.View.ReportForm for several 
                app.reportForm = new OWA.View.ReportForm({ model : ( new OWA.Model.Report() ) });
                app.reportForm.render();
                app.reportForm.on('cancel', function(){
                    Backbone.history.navigate('/')
                })
            });

            app.router.on('route:edit', function(id) {
                console.log('route:edit');
                var report = app.page.reports.get(id)
                app.reportForm = new OWA.View.ReportForm({ model : report });
                app.reportForm.render();
                app.reportForm.on('cancel', function(){
                    Backbone.history.navigate('/')
                })
            });

            Backbone.history.start();
        })
    }
);
