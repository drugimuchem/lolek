OWA.Page.Reporting = Backbone.View.extend({
	el: "#app-wrapper",

	events: {
		'keypress #report-index-new'                              : 'createOnEnter',
		'keyup #report-index-search input[data-search="name"]'    : 'search',
		'click #report-index-search input[data-search="public"]'  : 'search',
		'click #report-index-search input[data-search="private"]' : 'search'
	},

	initialize: function() {

        var self = this;

        this.render();

		this.reports = new OWA.Collection.Report();
		this.reports.on('add', this.addOne, this);
		this.reports.on('reset', this.renderList, this);

		this.input = $('#report-index-new');

		this.reports.fetch({
            success : function() {
                self.trigger('ready');
            }
        });
	},

    render: function() {
        this.$el.html(this.template({}));
        return this;
    },

	//http://backbonefu.com/2011/08/filtering-a-collection-in-backbone-js/
	search : function() {
		var 
		isPublicChecked  = $('#report-index-search input[data-search="public"]')[0].checked,
		isPrivateChecked = $('#report-index-search input[data-search="private"]')[0].checked,
		nameVal          = $('#report-index-search input[data-search="name"]')[0].value.trim().toLowerCase(),
        filterFunc;

        if (isPublicChecked && isPrivateChecked) {
            filterFunc =  function(item) { 
                return item.get('name').toLowerCase().indexOf(nameVal) > -1 
            };
        } else if (!isPublicChecked && !isPrivateChecked) {
            filterFunc =  function(item) { 
                return false;
            };
        } else {
            var isPublicExpected = isPublicChecked ? 1 : 0;
            filterFunc = function(item) { 
                return item.get('is_public') == isPublicExpected && item.get('name').toLowerCase().indexOf(nameVal) > -1;
            }
        }

        this.renderList(this.reports.search({ filter : filterFunc }));
	},

	addOne: function(report) {
		var view = new OWA.View.Report({
			model: report
		});
		$('#report-index').append(view.render().el)
	},

    renderList : function(reports) {
        $('#report-index').html('');
        var items = reports || this.reports;
        items.each(this.addOne,this)
        this.currentReports = items;
	},

	createOnEnter: function(e) {
		if (e.which !== ENTER_KEY || ! this.input.val().trim()) {
			return;
		}

		this.reports.create({ name : this.input.val().trim() });
		this.input.val('');
	}
});

