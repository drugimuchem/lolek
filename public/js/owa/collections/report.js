
OWA.Collection.Report = Backbone.Collection.extend({
    url    : '/reports',
    model  : OWA.Model.Report,

    search : function(finder) {
        if (finder.hasOwnProperty('where')) {
            return _( this.where(finder.where) );
        } else if (finder.hasOwnProperty('filter')) {
            return _( this.filter(finder.filter) );
        } else {
            throw 'not a proper collection finder'
        }
    }
})
